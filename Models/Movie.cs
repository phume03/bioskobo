﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bioskobo
{
    public partial class Movie
    {
        public int Id { get; set; }
        [Display(Name ="Movie Title: ")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Title { get; set; } = null!;
        [Display(Name = "Release Date: ")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ReleaseDate { get; set; }
        [RegularExpression(@"^[A-Z]+[a-zA-Z-\s]*$")]
        [Required]
        [StringLength(30)]
        [Display(Name = "Genre: ")]
        public string? Genre { get; set; } = null!;
        [Range(1,100)]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]
        [Display(Name ="Price (SZL): ")]
        public decimal Price { get; set; }
        [RegularExpression(@"^[A-Z]+[a-zA-Z0-9""'\s-]*$")]
        [Required]
        [StringLength(5)]
        [Display(Name = "Rating: ")]
        public string? Rating { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date Created: ")]
        public DateTime? DateCreated { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date Modified: ")]
        public DateTime? DateModified { get; set; }
    }
}
