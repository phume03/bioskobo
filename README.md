# BIOSKOBO #

This is a rehash of the ASP .Net Core 5.0 Tutorial. The main goal was to make an attempt at Database-First Development.
Following that tutorial, and the entity framework documentation; I started a new .net6.0 MVC project; I created a MOVIEMANAGER Database (BioskoboDBSetup.sql); I used entityframework ````dotnet ef dbcontext scaffold Name=ConnectionString```` to scaffold a database context and model; I updated the model with the necessary basic annotations and scaffolded the controller as well as views for it; and then ran the resultant application succesfully.

The next challenge is following the tutorial to further update the model, and or edit the model to include desired attributes. Such that the start of the project is assuredly db-first.


### Determine your EF Core "flavor" ###

There are several approaches to how EF Core works with your domain model and database implementation. In general, most apps will follow one of these patterns and how you approach your port will depend on the application "flavor".

**Code as the source of truth** is an approach in which everything is modeled through code and classes, whether through data attributes, fluent configuration, or a combination of both. The database is initially generated based on the model defined in EF Core and further updates are typically handled through migrations. This is often referred to as "code first," but the name isn't entirely accurate because one approach is to start with an existing database, generate your entities, and then maintain with code moving forward.

The **Database as source of truth** approach involves reverse-engineering or scaffolding your code from the database. When schema changes are made, the code is either regenerated or updated to reflect the changes. This is often called "database first."

Finally, a more advanced **Hybrid mapping approach follows the philosophy that the code and database are managed separately, and EF Core is used to map between the two. This approach typically eschews migrations.

The following table summarizes some high level differences:

| Approach | Developer role | DBA role | Migrations | Scaffolding | Repo |
| -------- | -------------- | -------- | ---------- | ----------- | ---- |
| Code first | Design entities and verify/customize generated migrations |	Verify schema definitions and changes |	Per commit | N/A |	Track entities, DbContext, and migrations |
| Database first |	Reverse engineer after changes and verify generated entities |	Inform developers when the database changes to re-scaffold |	N/A |	Per schema change |	Track extensions/partial classes that extend the generated entities |
| Hybrid |	Update fluent configuration to map whenever entities or database change |	Inform developers when the database has changed so they can update entities and model configuration |	N/A |	N/A |	Track entities and DbContext |


The hybrid approach is a more advanced approach with additional overhead compared to the traditional code and database approaches.



### Updating the Model ###

> After making changes to the database,you may need to update your EF Core model to reflect those changes. If
> the database changes are simple, it may be easiest just to manually make the changes to your EF Core model.
> For example, renaming a table or column, removing a column, or updating a column's type are trivial changes to
> makein code.
> More significant changes, however, are not as easy to make manually. One common workflow is to reverse
> engineer the model from the database again using -Force (PMC) or --force (CLI) to overwrite the existing
> model with an updated one.
> Another commonly requested feature is the ability to update the model from the database while preserving
> customization like renames, type hierarchies, etc. Use issue #831 to track the progress of this feature.

_**Warning:** If you reverse engineer the model from the database again, any changes you've made to the files will be lost._



### Updating Entities ###

This is a very basic project and there is a lot to read about updating entities based on the complexity of your project. But there are basic things to know and understand about it all -- when saving movie data, I found it necessary to keep track of some datetime information, some of which was not updated on edits. Thusly, I needed to track the old information.
The [query tracking section](https://docs.microsoft.com/en-us/ef/core/querying/tracking) of entity framework was useful towards achieving this goal.



### Publish to Azure ###

Microsoft Documentation provides [instructions](https://docs.microsoft.com/en-us/azure/app-service/tutorial-dotnetcore-sqldb-app?tabs=azure-portal%2Cvisualstudio-deploy%2Cdeploy-instructions-azure-portal%2Cazure-portal-logs%2Cazure-portal-resources) on how to publish your apps to their AZURE cloub platform. Hopefully, these steps will be translatable to other servers and databases.



## Resources ##

* [Entity Framework Core](https://docs.microsoft.com/en-us/ef/)
* [ASP Net Core 6.0](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-6.0)
* [Dot Net](https://docs.microsoft.com/en-us/dotnet/)
* [C#](https://docs.microsoft.com/en-us/dotnet/csharp/)
* [NuGet](https://docs.microsoft.com/en-us/nuget/)
* [SQL Server](https://docs.microsoft.com/en-us/sql/?view=sql-server-ver15)
* [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/?view=vs-2022)
* [C# and .NET](https://www.learnhowtoprogram.com/c-and-net)
* [StackOverflow: EF Tool](https://stackoverflow.com/questions/57066856/command-dotnet-ef-not-found)