DROP DATABASE IF EXISTS MOVIEMANAGER
	GO

CREATE DATABASE MOVIEMANAGER
	GO

USE MOVIEMANAGER
	GO

DROP TABLE IF EXISTS MOVIE
	GO

CREATE TABLE MOVIE (Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL, Title VARCHAR(100) NOT NULL, ReleaseDate DATETIME NULL, Genre VARCHAR(50) NULL, Price Decimal(6,2) NOT NULL, DateCreated DATETIME, DateModified DATETIME)
	GO

INSERT INTO MOVIE (Title, ReleaseDate, Genre, Price, DateCreated, DateModified)
	VALUES ('When Harry Met Sally', '1989-2-12', 'Romantic Comedy',7.99,SYSDATETIME(),SYSDATETIME())
	GO

INSERT INTO MOVIE (Title, ReleaseDate, Genre, Price, DateCreated, DateModified)
	VALUES ('Ghostbusters', '1984-3-13', 'Comedy',8.99,SYSDATETIME(),SYSDATETIME())
	GO

INSERT INTO MOVIE (Title, ReleaseDate, Genre, Price, DateCreated, DateModified)
	VALUES ('Ghostbusters 2', '1986-2-23', 'Comedy',9.99,SYSDATETIME(),SYSDATETIME())
	GO

INSERT INTO MOVIE (Title, ReleaseDate, Genre, Price, DateCreated, DateModified)
	VALUES ('Rio Bravo', '1959-4-15', 'Western',3.99,SYSDATETIME(),SYSDATETIME())
	GO

INSERT INTO MOVIE (Title, ReleaseDate, Genre, Price, DateCreated, DateModified)
	VALUES ('The Revenant', '2015-10-23', 'Revenge',10.99,SYSDATETIME(),SYSDATETIME())
	GO

SELECT * FROM MOVIE
	GO

ALTER TABLE MOVIE
	ADD Rating VARCHAR(5) NULL
	GO

